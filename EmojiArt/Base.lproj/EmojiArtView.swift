//
//  EmojiArtView.swift
//  EmojiArt
//
//  Created by Lubo on 3.06.19.
//  Copyright © 2019 Tumba. All rights reserved.
//

import UIKit

class EmojiArtView: UIView {

    var backgroundImage: UIImage? {
        didSet {
            setNeedsDisplay()
        }
    }

    override func draw(_ rect: CGRect) {
        backgroundImage?.draw(in: bounds)
    }

}
